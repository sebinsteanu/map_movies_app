package Model;

import Repository.HasId;

import java.io.Serializable;

/**
 * Created by sebi on 10/13/2016.
 */
public class Client implements Serializable, HasId<Integer>{
    private int id;
    private String nume,adresa;

    public Client() {
    }

    public Client(int idClient, String nume, String adresa) {
        this.id = idClient;
        this.nume = nume;
        this.adresa = adresa;
    }

    @Override
    public Integer getID() {
        return id;
    }

    @Override
    public void setID(Integer idClient) {
        this.id = idClient;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String toString(){
        return this.id+" "+nume+" "+adresa;
    }
}

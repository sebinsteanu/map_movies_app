package Model;

import Repository.HasId;
import Repository.ValidationException;

/**
 * Created by sebi on 10/13/2016.
 */
public class Movie implements HasId<Integer>{
    public Movie() {
    }

    private int idFilm;
    private String gen, regizor, titlu;


    public Movie(int idFilm, String gen, String regizor, String titlu) {
        this.idFilm = idFilm;
        this.gen = gen;
        this.regizor = regizor;
        this.titlu = titlu;
    }

    public String getTitlu() {
        return titlu;
    }
    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }
    public String getGen() {
        return gen;
    }
    public void setGen(String gen) {
        this.gen = gen;
    }

    public String getRegizor() {
        return regizor;
    }
    public void setRegizor(String regizor) {
        this.regizor = regizor;
    }

    @Override
    public Integer getID() {return idFilm;}
    @Override
    public void setID(Integer idFilm) {
        this.idFilm = idFilm;
    }

    @Override
    public String toString(){
        return this.idFilm+"|"+this.titlu+"|"+this.gen+"|"+this.regizor+"\n";
    }

    /**
     * Compare two entities
     * @param o1
     * @param o2
     * @return 0 if entities are the same
     *         !=0 if entities are different
     */
    public int compare(Movie o1, Movie o2) {
        return o1.getTitlu().compareTo(o2.getTitlu());
    }

    /**
     * Validate the current object
     */
    public void validate(){
        if (idFilm < 0)
            throw new ValidationException("Invalid ID");

        if (regizor == null || regizor == "")
            throw new ValidationException("Invalid regizor");

        if (gen == null || gen == "")
            throw new ValidationException("Invalid gen");

        if (titlu == null || titlu == "")
            throw new ValidationException("Invalid titlu");
    }
}

package Model;

import Repository.HasId;

import java.util.Date;

/**
 * Created by sebi on 12/7/2016.
 */
public class InchiriereFilm implements HasId<Integer> {
    private int idRent, idMovie, idClient;
    private String date;

    public InchiriereFilm(int idRent, int idClient, int idMovie, String date) {

        this.idRent = idRent;
        this.idMovie = idMovie;
        this.idClient = idClient;
        this.date = date;
    }

    @Override
    public Integer getID() {
        return this.idRent;
    }

    @Override
    public void setID(Integer integer) {
        this.idRent = integer;
    }

    public int getIdMovie() {
        return idMovie;
    }

    public void setIdMovie(int idMovie) {
        this.idMovie = idMovie;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}

package Repository;

import Model.InchiriereFilm;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Comparator;

/**
 * Created by sebi on 12/7/2016.
 */
public class FileRepoInchirieriFilme extends AbstractCrudRepository<Integer,InchiriereFilm> {
        private String numeFis;
        public FileRepoInchirieriFilme(String fisier, Validator<InchiriereFilm> val){
            super(val);
            numeFis = fisier;
            loadFromFile();
        }
        private void loadFromFile(){
            try {
                try (BufferedReader reader = new BufferedReader(new FileReader(numeFis))){
                    String line;
                    while ((line = reader.readLine()) != null) {
                        String[] el = line.split("[|]");
                        entities.add(new InchiriereFilm(Integer.parseInt(el[0]), Integer.parseInt(el[1]), Integer.parseInt(el[2]), el[3]));
                    }
                }
            }
            catch (IOException e){
                throw new RepositoryException(e.getMessage());
            }
        }

        private void writeToFile(){
            try{
                try (PrintWriter pw = new PrintWriter(numeFis)){
                    for (InchiriereFilm m: entities){
                        pw.println(m.getID()+"|"+m.getIdClient()+"|"+m.getIdMovie()+"|"+m.getDate());
                    }
                }
            }
            catch (IOException e){
                throw new RepositoryException(e.getMessage());
            }
        }

        @Override
        public void save(InchiriereFilm m) throws ValidationException
        {
            super.save(m);
            writeToFile();
        }
        @Override
        public void delete(Integer id) throws ValidationException
        {
            super.delete(id);
            writeToFile();
        }

        public int getMaxId() {
            return entities.stream().map(e->e.getID()).max(Integer::compareTo).get();
        }
}

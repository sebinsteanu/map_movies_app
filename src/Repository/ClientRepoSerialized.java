package Repository;

import Model.Client;
import utils.ClientEvent;
import utils.Event;
import utils.Observer;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

/**
 * Created by sebi on 11/12/2016.
 */
public class ClientRepoSerialized extends AbstractCrudRepository<Integer,Client> implements Observer<Event>{
    private String numeFis;
    public ClientRepoSerialized(String fisier, Validator<Client> val){
        super(val);
        numeFis = fisier;
        //if (fisier.isEmpty()) {
        //writeToFile();
        //}
        loadFromFile();
    }

    private void loadFromFile(){
        try {
            try (ObjectInputStream in = new ObjectInputStream(new FileInputStream(numeFis))){
                entities = (LinkedList<Client>) in.readObject();
            }
        }
        catch (Exception e){
            throw new RepositoryException(e.getMessage());
        }
    }

    private void writeToFile(){
        try (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(numeFis))){
            out.writeObject(entities);
        }
        catch (Exception e){
            throw new RepositoryException(e.getMessage());
        }
    }


    @Override
    public void save(Client c)
    {
        super.save(c);
        writeToFile();
    }
    @Override
    public void delete(Integer id) throws RepositoryException
    {
        super.delete(id);
        writeToFile();
    }

    @Override
    public void update(Integer id,Client c) throws RepositoryException
    {
        super.update(id,c);
        writeToFile();
    }

    @Override
    public void update(Event event) {
        writeToFile();
    }
}

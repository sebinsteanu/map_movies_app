package Repository.xml;

import Model.Movie;
import Repository.RepositoryException;
import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sebi on 12/16/2016.
 */
public class MovieXmlEntityReader  {



    public static <E> List<E> readList(Class<E> clazz, String filename, XMLEntityReader<E> entityReader) {
        XMLInputFactory factory = XMLInputFactory.newFactory();
        List<E> entities = null;
        String entityTag = clazz.getSimpleName().toLowerCase();
        String rootTag = entityTag + "s";
        try {
            InputStream fileInputStream = new FileInputStream(filename);
            if (fileInputStream == null) {
                String message = filename + " file not found";
                throw  new RepositoryException(message);
            }
            XMLStreamReader reader = factory.createXMLStreamReader(fileInputStream);

            while (reader.hasNext()) {
                reader.next();
                int eventType = reader.getEventType();
                String elementName = null;
                switch (eventType) {
                    case XMLStreamReader.START_ELEMENT:
                        elementName = reader.getLocalName();
                        if (elementName.equals(rootTag)) {
                            entities = new ArrayList<>();
                        } else if (elementName.equals(entityTag)) {
                            entities.add(entityReader.read(reader));
                        }
                        break;
                }
            }
            reader.close();
        } catch (XMLStreamException e) {
            throw new RepositoryException(e.getMessage());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } /*catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        return entities;
    }

}

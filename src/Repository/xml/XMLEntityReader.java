package Repository.xml;

import javax.xml.stream.XMLStreamReader;

/**
 * Created by sebi on 12/16/2016.
 */
public interface XMLEntityReader<E> {
    E read(XMLStreamReader reader);
}

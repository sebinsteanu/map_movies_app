package Repository.xml;

import Model.Movie;
import Repository.RepositoryException;
import jdk.internal.org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sebi on 12/15/2016.
 */
public class MovieHandler extends org.xml.sax.helpers.DefaultHandler {
    private String current;
    private Movie movie;
    private List<Movie> result;

    public MovieHandler() {
        current=null;
        movie=null;
        result=null;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        String elName=localName;
        if ("".equals(elName))
            elName=qName;
        current=elName;
        if ("movies".equals(current)){
            result=new ArrayList<Movie>();
            return;
        }
        if ("movie".equals(current)){
            movie=new Movie();
            String id=attributes.getValue("id");
            movie.setID(getIntegerNumber(id));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String s=new String(ch,start,length);
        if ("gen".equals(current)){
            movie.setGen(s);
        }
        if ("title".equals(current)) {
            movie.setTitlu(s);
        }
        if ("regizor".equals(current)) {
            movie.setRegizor(s);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        String elName=localName;
        if ("".equals(elName))
            elName=qName;
        if ("movie".equals(elName))
            result.add(movie);
        current=null;
    }


    private int getIntegerNumber(String val){
        try{
            return Integer.parseInt(val);
        }catch (NumberFormatException e){
            throw new RepositoryException("Error in xml file, numbered expected "+e.getMessage());
        }
    }

    public List<Movie> getMovies(){
        return result;
    }


    @Override
    public void error(SAXParseException e) throws SAXException {
        throw new RepositoryException("Eroare parsare fisier XML "+e.getMessage());
    }


}

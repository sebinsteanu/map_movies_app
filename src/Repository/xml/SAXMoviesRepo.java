package Repository.xml;

import Model.Movie;
import Repository.*;
import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;

import javax.xml.XMLConstants;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.stax.StAXSource;
import javax.xml.validation.*;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.List;

/**
 * Created by sebi on 12/15/2016.
 */
public class SAXMoviesRepo extends AbstractCrudRepository<Integer, Movie> {
    private String filename;
    private static final String schemaFile="movies.xsd";
    private boolean isValidating=false;

    public SAXMoviesRepo(Repository.Validator<Movie> valid, String filename) {
        super(valid);
        this.filename=filename;
        readFromXML();
    }

    private void readFromXML(){


        if (isValidating)
            validateFile();
        List<Movie> result= MovieXmlEntityReader.readList(Movie.class, filename, new SortingTaskXmlEntityReader());
        for(Movie p:result) {
            super.save(p);
        }
        //
        //SAXParserFactory factory = SAXParserFactory.newInstance();
        //factory.setValidating(true);
        //factory.setNamespaceAware(true);
        //MovieHandler handler = new MovieHandler();
        //try{
        //SAXParser parser = factory.newSAXParser();
        //File file = new File(filename);
        //parser.parse(file,handler);
        //List<Movie> list = handler.getMovies();
        //for(Movie m : list)
        //super.save(m);
        //}
        //catch (Exception e ){
        //System.out.println("Eroare la citire din XML");
        //}
    }
    @Override
    public void save(Movie entity) {
        for (Movie m: super.findAll())
            if(m.getID() == entity.getID()) {
                return;
            }
        super.save(entity);
        writeToFile();
    }

    @Override
    public void delete(Integer integer) {
        super.delete(integer);
        writeToFile();
    }

    private void validateFile(){
        XMLInputFactory factory = XMLInputFactory.newFactory();
        SchemaFactory factorys = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = null;
        try {
            XMLStreamReader reader = factory.createXMLStreamReader(new FileInputStream(filename));

            schema = factorys.newSchema(new File(schemaFile));
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.validate(new StAXSource(reader));
            System.out.println("passed validation");
        } catch (Exception e) {
            throw new RepositoryException("Error validating file "+e.getMessage());
        }
    }


    private void writeToFile(){
        XMLOutputFactory xof =  XMLOutputFactory.newInstance();
        try {
            XMLStreamWriter xtw = new IndentingXMLStreamWriter(xof.createXMLStreamWriter(new FileWriter(filename)));


            xtw.writeStartDocument("utf-8", "1.0");
            xtw.writeStartElement( "movies");
            if (isValidating){
                xtw.writeDefaultNamespace("urn:Movies.namespace");
                xtw.writeNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance");
                xtw.writeAttribute("http://www.w3.org/2001/XMLSchema-instance","schemaLocation","urn:Movies.namespace "+schemaFile);
            }
            for(Movie movie:findAll()){
                xtw.writeStartElement("movie");
                xtw.writeAttribute("id", ""+movie.getID());

                xtw.writeStartElement("gen");
                xtw.writeCharacters(movie.getGen());
                xtw.writeEndElement();

                xtw.writeStartElement("regizor");
                xtw.writeCharacters(movie.getRegizor());
                xtw.writeEndElement();

                xtw.writeStartElement("title");
                xtw.writeCharacters(movie.getTitlu());
                xtw.writeEndElement();

                xtw.writeEndElement();
            }
            xtw.writeEndElement();
            xtw.writeEndDocument();
            xtw.flush();
            xtw.close();
        }catch (Exception e){
            System.out.println("Exception saving xml file "+e);
            e.printStackTrace();
        }
    }

    private static  class SortingTaskXmlEntityReader implements XMLEntityReader<Movie> {
        String REGIZOR = "regizor";
        String GEN = "gen";
        String ID = "id";
        String TITLU = "title";


        @Override
        public Movie read(XMLStreamReader reader) {
            Movie movie = new Movie();
            String entityElementName = reader.getLocalName();
            try {
                movie.setID(Integer.parseInt(reader.getAttributeValue(null, ID)));
                String currentElementName = null;
                boolean completed = false;
                do {
                    int eventType = reader.getEventType();
                    switch (eventType) {
                        case XMLStreamReader.START_ELEMENT:
                            currentElementName = reader.getLocalName();
                            break;
                        case XMLStreamReader.END_ELEMENT:
                            currentElementName = reader.getLocalName();
                            if (currentElementName.equals(entityElementName)) {
                                completed = true;
                            }
                            break;
                        case XMLStreamReader.CHARACTERS: {
                            if (reader.isWhiteSpace())
                                break;
                            if (REGIZOR.equals(currentElementName))
                                movie.setRegizor(reader.getText().trim());
                            if (GEN.equals(currentElementName))
                                movie.setGen(reader.getText().trim());
                            if (TITLU.equals(currentElementName))
                                movie.setTitlu(reader.getText().trim());
                            break;
                        }
                    }
                    reader.next();
                } while (!completed && reader.hasNext());
            } catch (Exception e) {
                throw new RepositoryException(e.getMessage());
            }

            return movie;
        }
    }

}

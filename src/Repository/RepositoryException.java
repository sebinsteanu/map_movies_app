package Repository;

/**
 * Created by sebi on 10/13/2016.
 */
public class RepositoryException extends RuntimeException {
    public RepositoryException(String message) {
        super(message);
    }
}

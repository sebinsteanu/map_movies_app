package Repository;

/**
 * Created by sebi on 10/26/2016.
 */

public interface Validator<E> {
    void validate(E entity);
}

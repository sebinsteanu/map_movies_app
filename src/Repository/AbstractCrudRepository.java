package Repository;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sebi on 10/27/2016.
 */
public abstract class AbstractCrudRepository<ID, E extends HasId<ID>> implements CrudRepository<ID,E> {
    protected LinkedList<E> entities;
    protected Validator<E> val;

    public AbstractCrudRepository(Validator<E> val) {
        entities = new LinkedList<E>();
        this.val = val;
    }
    @Override
    public long size(){
        return this.entities.size();
    }
    @Override
    public void save(E el){
        try{
            val.validate(el);
        }
        catch(ValidationException e){
            throw new RepositoryException(e.toString());
        }
        entities.add(el);
    }
    @Override
    public void delete(ID id){
        if (getById(id) != null) {
            entities.remove(getById(id));
        }
        else throw  new RepositoryException("Id-ul nu e in lista momentan!");
    }
    @Override
    public E getById(ID id){
        for (E el: entities)
            if (el.getID() == id)
                return el;
        return null;
    }

    @Override
    public void update(ID id,E el){
        E e = getById(id);
        if (e!= null){
            entities.remove(e);
            entities.add(el);
        }
        else throw  new RepositoryException("elementul nu e in lista!");
    }

    public Iterable<E> findAll(){
        return this.entities;
    }
}

package Repository;

/**
 * Created by sebi on 10/25/2016.
 */
public interface CrudRepository<ID,E> {
    public long size();
    public void save(E entity);
    public void delete(ID id);
    public void update(ID id, E el);
    public E getById(ID id);
    public Iterable<E> findAll();
}

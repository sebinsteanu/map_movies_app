package Repository;

import Model.InchiriereFilm;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by sebi on 12/7/2016.
 */
public class ValidatorInchiriere implements Validator<InchiriereFilm> {
    @Override
    public void validate(InchiriereFilm entity) {
        if( entity.getID() < 0 || entity.getIdClient() < 0 || entity.getIdMovie() < 0)
            throw new RepositoryException("Id-ul nu poate fi negativ!\n");

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            sdf.parse(entity.getDate());
        }
        catch (ParseException e){
            throw new RepositoryException("Data nu este valida: "+e.getMessage());
        }
    }
}

package Repository;

/**
 * Created by sebi on 10/13/2016.
 */
public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }
}

package Repository;

/**
 * Created by sebi on 12/15/2016.
 */

import Model.Movie;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import javax.xml.stream.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class SectionData
{
    private ArrayList<Movie> entities = new ArrayList<>();
    private String fileName;

    public SectionData(String file)
    {
        this.entities = new ArrayList<>();
        this.fileName = file;

        try {
            this.readFromFile();
        } catch (Exception e) {
            System.out.println("Cannot read from section file!");
        }
    }

    private ArrayList<Movie> readXML(XMLStreamReader reader)
    {
        ArrayList<Movie> sections = new ArrayList<>();
        int id = 0;
        String gen = "", regizor = "", titlu = "";

        try {
            while (reader.hasNext()) {
                Integer currentEvent = reader.next();
                if (currentEvent == XMLStreamReader.START_ELEMENT) {
                    if (reader.getLocalName().equals("section")) {
                        reader.next();

                        if (reader.getLocalName().equals("id")) {
                            id = Integer.parseInt(reader.getElementText());
                            reader.next();
                        }

                        if (reader.getLocalName().equals("gen")) {
                            gen = reader.getElementText();
                            reader.next();
                        }
                        if (reader.getLocalName().equals("regizor")) {
                            regizor = reader.getElementText();
                            reader.next();
                        }
                        if (reader.getLocalName().equals("titlu")) {
                            titlu = reader.getElementText();
                            reader.next();
                        }
                    }
                } else if (currentEvent == XMLStreamReader.END_ELEMENT) {
                    if (reader.getLocalName().equals("section") && id > 0) {
                        sections.add(new Movie(id, gen, regizor, titlu));
                    }
                }
            }
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

        return sections;
    }

    private void readFromFile() throws FileNotFoundException
    {
        InputStream inputStream = new FileInputStream(this.fileName);
        XMLInputFactory factory = XMLInputFactory.newFactory();

        try {
            XMLStreamReader reader = factory.createXMLStreamReader(inputStream);
            this.entities = this.readXML(reader);
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void writeXML(XMLStreamWriter writer) throws IOException
    {
        try {
            writer.writeStartDocument();
            writer.writeStartElement("sections");

            for (Movie s : this.entities) {
                writer.writeStartElement("section");

                writer.writeStartElement("id");
                writer.writeCharacters(s.getID().toString());
                writer.writeEndElement();

                writer.writeStartElement("gen");
                writer.writeCharacters(s.getGen() + "");
                writer.writeEndElement();

                writer.writeStartElement("regizor");
                writer.writeCharacters(s.getRegizor());
                writer.writeEndElement();

                writer.writeStartElement("titlu");
                writer.writeCharacters(s.getTitlu());
                writer.writeEndElement();


                writer.writeEndElement();
            }

            writer.writeEndElement();
            writer.writeEndDocument();
            writer.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile()
    {
        OutputStream outputStream = null;
        XMLStreamWriter writer;

        try {
            outputStream = new FileOutputStream(this.fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        XMLOutputFactory factory = XMLOutputFactory.newFactory();

        try {
            factory.createXMLEventWriter(outputStream);
            writer = factory.createXMLStreamWriter(outputStream);
            this.writeXML(writer);
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public ObservableList<Movie> getSectionData() throws Exception
    {
        return FXCollections.observableArrayList(this.entities);
    }

    public ArrayList<Movie> getList()
    {
        return this.entities;
    }

    public void setList(ArrayList<Movie> list)
    {
        this.entities = list;
        try {
            this.writeToFile();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

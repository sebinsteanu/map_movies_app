package Repository;

/**
 * Created by sebi on 10/27/2016
 *
 * ///////////////////    to    delete   :D
 */
public interface HasId<ID> {
    public ID getID();
    public void setID(ID id);
}

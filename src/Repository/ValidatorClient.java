package Repository;

import Model.Client;

/**
 * Created by sebi on 11/12/2016.
 */
public class ValidatorClient implements Validator<Client> {

    @Override
    public void validate(Client c){
        if (c.getID() < 0)
            throw new ValidationException("Invalid ID");

        if (c.getNume() == null || c.getNume() == "")
            throw new ValidationException("Invalid regizor");

        if (c.getAdresa() == null || c.getAdresa() == "")
            throw new ValidationException("Invalid gen");
    }
}

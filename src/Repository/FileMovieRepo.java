package Repository;

import Model.Movie;
import utils.Event;
import utils.MovieEvent;
import utils.Observer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by sebi on 11/9/2016.
 */
public class FileMovieRepo extends MovieRepository implements Observer<Event> {
    private String numeFis;
    public FileMovieRepo(String fisier, Validator<Movie> val){
        super(val);
        numeFis = fisier;
        loadFromFile();
    }
    @Override
    public void update(Event event){
        writeToFile();
    }
    private void loadFromFile(){
        try {
            try (BufferedReader reader = new BufferedReader(new FileReader(numeFis))){
                String line;
                while ((line = reader.readLine()) != null) {
                    String[] el = line.split("[|]");
                    entities.add(new Movie(Integer.parseInt(el[0]), el[1], el[2], el[3]));
                }
            }
        }
        catch (IOException e){
            throw new RepositoryException(e.getMessage());
        }
    }

    private void writeToFile(){
        try{
            try (PrintWriter pw = new PrintWriter(numeFis)){
                for (Movie m: entities){
                    pw.println(m.getID()+"|"+m.getGen()+"|"+m.getRegizor()+"|"+m.getTitlu());
                }
            }
        }
        catch (IOException e){
            throw new RepositoryException(e.getMessage());
        }
    }


    @Override
    public void save(Movie m) throws ValidationException
    {
        super.save(m);
        writeToFile();
    }
    @Override
    public void delete(Integer id) throws ValidationException
    {
        super.delete(id);
        writeToFile();
    }

    @Override
    public void update(Integer id,Movie m) throws ValidationException
    {
        super.update(id,m);
        writeToFile();
    }

}

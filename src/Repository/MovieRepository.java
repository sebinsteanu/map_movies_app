package Repository;

import Model.Movie;

/**
 * Created by sebi on 10/27/2016.
 */
public class MovieRepository extends AbstractCrudRepository<Integer, Movie> {

    public MovieRepository(Validator<Movie> val) {
        super(val);
    }
}

package Repository;

import Model.Movie;

/**
 * Created by sebi on 10/26/2016.
 */
public class ValidatorMovie implements  Validator<Movie> {
    @Override
    public void validate(Movie m){
        if (m.getID() < 0)
            throw new ValidationException("Invalid ID");

        if (m.getRegizor() == null || m.getRegizor() == "")
            throw new ValidationException("Invalid regizor");

        if (m.getGen() == null || m.getGen() == "")
            throw new ValidationException("Invalid gen");
    }
}

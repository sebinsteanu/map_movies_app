package Test;

import Model.Client;
import Model.Movie;
import Repository.MovieRepository;
import Repository.RepositoryException;
import Repository.ValidatorMovie;

/**
 * Created by sebi on 10/13/2016.
 */
public class AppTest {

    public static void ClientTest(){
        Client c = new Client(1, "Paul Vieru", "Str Unirii, blocul fericirii");
        System.out.println(c.getID());
        System.out.println(c.getNume());
        System.out.println(c.getAdresa());
        c.setID(3);
        c.setNume("Mihaita din Berceni");
        c.setAdresa("Strada iubirii");
        System.out.println(c.getID());
        System.out.println(c.getNume());
        System.out.println(c.getAdresa());
    }

    public static void MovieTest() {
        Movie f = new Movie(1, "drama", "Steven Spielberg", "Titanic");
        System.out.println(f.getID());
        System.out.println(f.getGen());
        System.out.println(f.getRegizor());
        f.setID(3);
        f.setGen("horror");
        f.setRegizor("Paul Russeau");
        System.out.println(f.getID());
        System.out.println(f.getGen());
        System.out.println(f.getRegizor());
    }

    public static void testMovieRepository(){
        ValidatorMovie val = new ValidatorMovie();
        MovieRepository repo = new MovieRepository(val);

        Movie m = new Movie(5, "sdf", "dsfhgfhdfhg", "Barni");;

        try {
            repo.save(new Movie(1, "gen", "regizor", "Titanicul"));
            repo.save(new Movie(2, "gennn", "regizoio", "another"));
            repo.save(m);
            repo.save(new Movie(3, "gennnn", "regizzzzor", "Ice age"));
        } catch (RepositoryException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("\nBefore delete: ");
        for (Movie m2 : repo.findAll()) {
            System.out.print(m2);
        }
        repo.delete(m.getID());

        System.out.println("\nAfter delete: ");
        for (Movie m2 : repo.findAll()) {
            System.out.print(m2);
        }
        System.out.println("\n");
    }
}

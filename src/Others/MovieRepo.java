package Others;

import Model.Movie;

/**
 * Created by sebi on 10/13/2016.
 */
public class MovieRepo {
    private int cap, size;
    private Movie[] entities;

    public MovieRepo (){
        size =0;
        cap = 20;
        entities = new Movie[cap];
    }
    public long size() {
        return size;
    }
    public Movie[] getAll() {
        return entities;
    }
    public void add(Movie film) {
        if(size == cap)
            resize();
        entities[size] = film;
        size++;
    }
    private void resize(){
        cap = 2*cap;
        Movie[] aux = new Movie[cap];
        for(int i=0;i<size;i++)
            aux[i]=entities[i];
        entities = aux;
    }
    public void remove(Movie film) {
        for(int i=0;i<size;i++){
            if(entities[i].getID() == film.getID()){
                for(int j=i;j<size-1;j++)
                    entities[j]=entities[j+1];
                size--;
                break;
            }
        }

    }
    public void modify(Movie film) {
        for(int i=0;i<size;i++){
            if(entities[i].getID() == film.getID()){
                entities[i]=film;
                break;
            }
        }

    }
    public Movie getById(int id) {
        for(int i=0;i<size;i++)
            if(entities[i].getID() == id)
                return entities[i];
        return null;
    }

    public boolean contains(int id) {
        for(int i=0;i<size;i++)
            if(entities[i].getID() == id)
                return true;
        return false;
    }
}

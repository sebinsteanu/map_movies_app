package service;

import Model.Movie;
import Repository.FileMovieRepo;
import Repository.MovieRepository;
import Repository.RepositoryException;
import utils.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sebi on 12/3/2016.
 */
public class MVCMovieService implements Observable<Event> {

    private List<Observer<Event>> observers=new ArrayList<>();
    private FileMovieRepo repo;

    public MVCMovieService(FileMovieRepo repo){
        this.repo=repo;
        addObserver(this.repo);
    }

    /**
    public TaskService(MovieRepository repo, ObservableTaskRunner runner){
        this.repo=repo;
        this.runner=runner;
    }*/
    public void addMovie(Movie movie) throws RepositoryException{
        if(repo.getById(movie.getID()) != null)
            throw new RepositoryException("Specified film does already exist");

        repo.save(movie);
        notifyObservers(new MovieEvent(METype.ADD,movie));
    }
    public void deleteMovie(Movie movie){
        repo.delete(movie.getID());
        notifyObservers(new MovieEvent(METype.DELETE, movie));
    }
    public void updateMovie(Movie oldmovie, Movie newmovie) {
        repo.update(oldmovie.getID(),newmovie);
        notifyObservers(new MovieEvent(METype.UPDATE,newmovie,oldmovie));
    }

    public Iterable<Movie> getAll(){
        return repo.findAll();
    }
    public Movie getById(int id){return repo.getById(id);}

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    /**
    public void addRunnerObserver(Observer<movieEvent> obs){
        runner.addObserver(obs);
    }
    public void removeRunnerObserver(Observer<movieEvent> obs){
        runner.removeObserver(obs);
    }*/
    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event event) {
        observers.stream().forEach(x->x.update(event));
    }

    /**{
     public void addmovieToRunner(int movieId){
     Movie movie=repo.getById(movieId);
     runner.addmovie(movie);
     }
     public void executeOnemovie(){
     runner.executeOnemovie();
     }

     public void executeAll(){
     runner.executeAll();
     }}*/
}

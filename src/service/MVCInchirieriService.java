package service;

import Model.InchiriereFilm;
import Repository.FileRepoInchirieriFilme;
import Repository.RepositoryException;
import utils.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sebi on 12/7/2016.
 */
public class MVCInchirieriService implements Observable<Event> {
    private FileRepoInchirieriFilme repo;
    private List<Observer<Event>> observers = new ArrayList<>();

    public MVCInchirieriService(FileRepoInchirieriFilme repo) {
        this.repo = repo;
    }
    public void addInchiriere(InchiriereFilm inchiriereFilm){
        for(InchiriereFilm inchiriere: repo.findAll())
            if (inchiriere.getIdMovie()==inchiriereFilm.getIdMovie() && inchiriere.getIdClient() == inchiriereFilm.getIdClient())
                throw new RepositoryException("Perechea de id-uri asociate inchirierii este deja in lista");
        repo.save(inchiriereFilm);
        notifyObservers(new InchiriereEvent(METype.ADD,inchiriereFilm));
    }
    public void deleteInchiriere(InchiriereFilm inchiriereFilm) {
        for (InchiriereFilm inchiriere : repo.findAll())
            if (inchiriere.getIdMovie() == inchiriereFilm.getIdMovie() && inchiriere.getIdClient() == inchiriereFilm.getIdClient()){
                repo.delete(inchiriere.getID());
                notifyObservers(new InchiriereEvent(METype.DELETE, inchiriereFilm));
                return;
            }
        throw new RepositoryException("Perechea de id-uri nu se afla in lista");
    }
    public Iterable<InchiriereFilm> getAll(){
        return repo.findAll();
    }
    public int getFreeId(){return repo.getMaxId()+1;}

    @Override
    public void addObserver(Observer<Event> observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer<Event> observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(o->o.update(t));
    }
}

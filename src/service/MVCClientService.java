package service;

import Model.Client;
import Repository.ClientRepoSerialized;
import Repository.RepositoryException;
import utils.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sebi on 12/5/2016.
 */
public class MVCClientService implements Observable<Event>{
    List<Observer<Event>> observers = new ArrayList<>();
    private ClientRepoSerialized repo;

    public MVCClientService(ClientRepoSerialized repo) {
        this.repo = repo;
        addObserver(this.repo);
    }
    public void addClient(Client client) throws RepositoryException {
        if (repo.getById(client.getID()) != null)
            throw new RepositoryException("Specified film does already exist");
        repo.save(client);
        notifyObservers(new ClientEvent(METype.ADD, client));
    }

    public void deleteClient(Client client){
        repo.delete(client.getID());
        notifyObservers(new ClientEvent(METype.DELETE, client));
    }

    public void updateClient(Client oldClient, Client newClient) {
        repo.update(oldClient.getID(),newClient);
        notifyObservers(new ClientEvent(METype.UPDATE,newClient,oldClient));
    }

    public Iterable<Client> getAll(){
        return repo.findAll();
    }
    public Client getById(int id){return repo.getById(id);}

    @Override
    public void addObserver(Observer<Event> observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(Observer<Event> observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(o->o.update(t));
    }

}

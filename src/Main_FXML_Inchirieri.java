import Repository.*;
import View.InchirieriViewFXML;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import service.MVCClientService;
import service.MVCInchirieriService;
import service.MVCMovieService;

/**
 * Created by sebi on 12/7/2016.
 */
public class Main_FXML_Inchirieri extends Application {
    public static void main(String[] args){
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("FXML View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View/inchirieri.fxml"));
        Pane pane = loader.load();

        InchirieriViewFXML my_app = loader.getController();
        my_app.setService(getServiceInchirieri(),getServiceClients(),getServiceMovies());

        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    static MVCInchirieriService getServiceInchirieri(){
        FileRepoInchirieriFilme repo = new FileRepoInchirieriFilme("D:\\map\\Movies_app\\src\\inchirieri.txt", new ValidatorInchiriere());
        MVCInchirieriService service = new MVCInchirieriService(repo);
        return service;
    }

    static MVCClientService getServiceClients(){
        ClientRepoSerialized repo = new ClientRepoSerialized("D:\\map\\Movies_app\\src\\fisierSerializat.txt", new ValidatorClient());
        MVCClientService service = new MVCClientService(repo);
        return service;
    }
    private static MVCMovieService getServiceMovies() {
        FileMovieRepo repo = new FileMovieRepo("D:\\map\\Movies_app\\src\\filme.txt", new ValidatorMovie());
        MVCMovieService service = new MVCMovieService(repo);
        return service;
    }
}

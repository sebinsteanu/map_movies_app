package Controller;

import Model.Movie;
import Repository.RepositoryException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.input.MouseEvent;
import service.MVCMovieService;
import utils.Event;
import utils.MovieEvent;
import utils.Observer;

import java.util.stream.Collectors;

/**
 * Created by sebi on 12/3/2016.
 */
public class MVCMovieController implements Observer<Event> {
    private MVCMovieService service;
    private ObservableList<Movie> moviesModel;

    public MVCMovieController(MVCMovieService service){
        this.service=service;
        service.addObserver(this);
        moviesModel = FXCollections.observableArrayList();
        populateList();
    }

    private void populateList(){
        moviesModel.clear();
        Iterable<Movie> movies = service.getAll();
        movies.forEach(x->moviesModel.add(x));
    }
    public void addMovie(int id, String titlu, String gen, String regizor) throws RepositoryException{
        Movie film = new Movie(id,gen,regizor, titlu);
        service.addMovie(film);
    }

    public void deletemovie(Movie movie){
        service.deleteMovie(movie);
    }
    public ObservableList<Movie> getmoviesModel(){
        return moviesModel;
    }
    public ObservableList<Movie> getmoviesByRegizor(String regizor){
        return moviesModel.stream().filter(x->x.getRegizor().equals(regizor)).collect(Collectors.toCollection(FXCollections::observableArrayList));
    }
    public ObservableList<Movie> getmoviesByGen(String gen){
        return moviesModel.stream().filter(x->x.getGen().equals(gen)).collect(Collectors.toCollection(FXCollections::observableArrayList));
    }

    public void updateMovie(Movie oldmovie, int id, String titlu, String gen, String regizor){
        Movie newmovie = new Movie(id,gen,regizor, titlu);
        newmovie.validate();
        service.updateMovie(oldmovie,newmovie);
    }

    @Override
    public void update(Event event) {
        MovieEvent movieEvent = (MovieEvent) event;
        switch (movieEvent.getType()){
            case ADD:{
                moviesModel.add(movieEvent.getData());
                break;
            }
            case DELETE:{
                moviesModel.remove(movieEvent.getData());
                break;
            }
            case UPDATE:{
                moviesModel.remove(movieEvent.getOldData());
                moviesModel.add(movieEvent.getData());
                break;
            }
        }
        populateList();
    }
}

package Controller;

import Model.Client;
import Model.Movie;
import Repository.*;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by sebi on 10/13/2016.
 */
public class MovieController {
    ValidatorMovie val = new ValidatorMovie();
    ValidatorClient valC = new ValidatorClient();
    ClientRepoSerialized clientRepoSerialized;
    MovieRepository filmRepository;

    public void setFilmRepository(MovieRepository filmRepository) {
        this.filmRepository = filmRepository;
    }
    public void setClientRepository(ClientRepoSerialized clientRepository) {
        this.clientRepoSerialized = clientRepository;
    }

    public Movie addFilm(int id, String titlu, String gen, String regizor){
        Movie film = new Movie(id,gen,regizor, titlu);
        film.validate();

        if(filmRepository.getById(id) != null)
            throw new RepositoryException("Specified film does already exist");

        filmRepository.save(film);
        return film;
    }

    public Movie removeFilm(int id){
        if(filmRepository.getById(id) == null)
            throw new RepositoryException("Specified film does not exist");
        Movie film = filmRepository.getById(id);
        filmRepository.delete(id);
        return film;
    }

    public Movie modifyFilm(int id, String titlu, String gen, String regizor){
        if(filmRepository.getById(id) == null)
            throw new RepositoryException("Specified film does not exist");
        Movie film = new Movie(id, titlu,gen,regizor);

        film.validate();
        filmRepository.update(id, film);
        return film;
    }

    //public Movie GetFilmByID(int id) {return filmRepository.getById(id);}

    public Iterable<Movie> GetAllFilms() {
        return filmRepository.findAll();
    }

    public Client addClient(int id, String nume, String adresa){
        Client client = new Client(id,nume,adresa);
        valC.validate(client);

        if(clientRepoSerialized.getById(id) != null)
            throw new RepositoryException("Specified client does already exist");

        clientRepoSerialized.save(client);
        return client;
    }

    public Client removeClient(int id){

        if(clientRepoSerialized.getById(id) == null)
            throw new RepositoryException("Specified client does not exist");

        Client client = clientRepoSerialized.getById(id);
        clientRepoSerialized.delete(client.getID());
        return client;
    }

    public Client modifyClient(int id, String nume, String adresa){
        if(clientRepoSerialized.getById(id) == null)
            throw new RepositoryException("Specified client does not exist");
        Client client = new Client();
        client.setID(id);
        client.setNume(nume);
        client.setAdresa(adresa);
        valC.validate(client);

        clientRepoSerialized.update(id, client);
        return client;

    }

    //public Client GetClientByID(int id) {return clientRepoSerialized.getById(id);}

    public Iterable<Client> GetAllClients() {
        return clientRepoSerialized.findAll();
    }

    //public long GetSize(){return filmRepository.size();}
    //public long GetClientsSize(){return clientRepoSerialized.size();}


    /*functia generica de filtrare
    parametrii intrare:  o lista generica, un predicat generic
    parametrii iesire: lista filtrata dupa predicatul primit
     */
    private  <E> List<E> filtrare(List<E> lista, Predicate<E> cond){
        List<E> rez = new LinkedList<E>() {
        };
        for(E el:lista){
            if(cond.test(el))
                rez.add(el);
        }
        return rez;
    }
    /*functia filtrare filme dupa regizor
    parametrii intrare:  o lista de filme, un string care contine numele regizorului film
    parametrii iesire: lista filtrata dupa regizorul cerut
     */

    /**
     *
     * @param lista
     * @param reg
     * @return
     */
    public List<Movie> filterByRegizor(List<Movie> lista, String reg){
        List<Movie> filteredList = filtrare(lista,(x)->{return x.getRegizor().equals(reg);});
        return filteredList;
    }
    /*functia filtrare filme dupa gen
    parametrii intrare:  o lista de filme, un string care contine numele genului filmului
    parametrii iesire: lista filtrata dupa genul cerut
     */
    public List<Movie> filterByGen(List<Movie> lista, String gen){
        List<Movie> filteredList = filtrare(lista,(x)->{return x.getGen().equals(gen);});
        return filteredList;
    }
    /*functia filtrare filme dupa nume
    parametrii intrare:  o lista de clienti, un string care contine numele clientului
    parametrii iesire: lista filtrata dupa numele cerut
     */
    public List<Client> filterByName(List<Client> lista, String name){
        List<Client> filteredList = filtrare(lista,(x)->{return x.getNume().equals(name);});
        return filteredList;
    }
    /*functia filtrare filme dupa adr
    parametrii intrare:  o lista de clienti, un string care contine adresa clientului
    parametrii iesire: lista filtrata dupa adresa cerut
     */
    public List<Client> filterByAddress(List<Client> lista, String adr){
        List<Client> filteredList = filtrare(lista,(x)->{return x.getAdresa().equals(adr);});
        return filteredList;
    }
}

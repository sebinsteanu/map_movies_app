package Controller;

import Model.Client;
import javafx.collections.ObservableList;
import service.MVCClientService;
import utils.ClientEvent;
import utils.Observer;


/**
 * Created by sebi on 12/5/2016.
 */
public class MVCClientController  implements Observer<ClientEvent>{
    MVCClientService service;
    ObservableList<Client> clientsModel;

    public MVCClientController(MVCClientService service) {
        this.service = service;
    }

    public void populateList(){
        Iterable<Client> clients = service.getAll();
        clients.forEach(c->clientsModel.add(c));
    }

    public Client addClient(int id, String nume, String adresa){
        Client client = new Client(id,nume,adresa);
        service.addClient(client);
        return client;
    }

    public Client removeClient(Client client){
        service.deleteClient(client);
        return client;
    }

    public Client updateClient(Client client,Integer id, String nume, String adresa){
        Client client2 = new Client(id,nume,adresa);
        service.updateClient(client,client2);
        return client;

    }
    public ObservableList<Client> getClientsModel() {
        return clientsModel;
    }

    @Override
    public void update(ClientEvent event) {
        switch (event.getType()){
            case ADD:{
                clientsModel.add(event.getData());
                break;
            }
            case DELETE:{
                clientsModel.remove(event.getData());
                break;
            }
            case UPDATE:{
                clientsModel.remove(event.getOldData());
                clientsModel.add(event.getData());
                break;
            }
        }
    }
}

import Controller.MVCMovieController;
import Repository.FileMovieRepo;
import Repository.ValidatorMovie;
import View.MoviesView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.MVCMovieService;

/**
 * Created by sebi on 12/2/2016.
 */
public class Main_FX_Movies extends Application{
    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Movies View");
        Scene scene;

        BorderPane layout = getView();
        scene = new Scene(layout, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private static BorderPane getView(){
        FileMovieRepo repo = new FileMovieRepo("D:\\map\\Movies_app\\src\\filme.txt", new ValidatorMovie());

        MVCMovieService service = new MVCMovieService(repo);
        MVCMovieController controller = new MVCMovieController(service);
        MoviesView view = new MoviesView(controller);
        return view.getView();
    }
}

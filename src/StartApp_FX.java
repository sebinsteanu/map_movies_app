import Controller.MVCMovieController;
import Repository.*;
import View.ClientiViewFXML;
import View.InchirieriViewFXML;
import View.MoviesView;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import service.MVCClientService;
import service.MVCInchirieriService;
import service.MVCMovieService;

/**
 * Created by sebi on 12/13/2016.
 */
public class StartApp_FX extends Application {
    public static void main(String[] args){
        launch(args);
    }
    @Override
    public void start(Stage primaryStage) throws Exception {

//
        //final ToggleGroup group = new ToggleGroup();
        //
        //ToggleButton tb1 = new ToggleButton("Minor");
        //tb1.setToggleGroup(group);
        //tb1.setSelected(true);
        //
        //ToggleButton tb2 = new ToggleButton("Major");
        //tb2.setToggleGroup(group);
        //
        //ToggleButton tb3 = new ToggleButton("Critical");
        //tb3.setToggleGroup(group);
        //
        //tb1.setUserData(Color.LIGHTGREEN);
        //tb2.setUserData(Color.LIGHTBLUE);
        //tb3.setUserData(Color.SALMON);
        //
        //final Rectangle rect = new Rectangle(145, 50);
        //
        //group.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
        //public void changed(ObservableValue<? extends Toggle> ov, Toggle toggle, Toggle new_toggle) {
        //if (new_toggle == null)
        //rect.setFill(Color.WHITE);
        //else
        //rect.setFill(
        //(Color) group.getSelectedToggle().getUserData()
        //);
        //}
        //});
        //HBox layout =new HBox();
        //layout.getChildren().addAll(tb1,tb2,tb3);
        //Scene scene = new Scene(new Group());
        //((Group) scene.getRoot()).getChildren().add(layout);
        //primaryStage.setScene(scene);
        //primaryStage.show();
        //
        MVCClientService clientsService = getClientsService();
        MVCMovieService movieService = getMoviesService();
        TabPane tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.UNAVAILABLE);

        Tab tabFilme = new Tab("Filme");
        MVCMovieController controller = new MVCMovieController(movieService);
        MoviesView view = new MoviesView(controller);
        tabFilme.setContent(view.getView());

        Tab tabClienti = new Tab("Clienti");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View/clienti.fxml"));
        Pane paneClienti = loader.load();
        ClientiViewFXML ctrl = loader.getController();
        ctrl.setService(clientsService);
        tabClienti.setContent(paneClienti);

        Tab tabInchirieri = new Tab("Inchirieri");
        FXMLLoader loaderInchirieri = new FXMLLoader(getClass().getResource("View/inchirieri.fxml"));
        Pane paneInchirieri = loaderInchirieri.load();
        InchirieriViewFXML inchirieriViewFXML = loaderInchirieri.getController();
        inchirieriViewFXML.setService(getServiceInchirieri(),clientsService,movieService);
        tabInchirieri.setContent(paneInchirieri);

        tabPane.getTabs().addAll(tabFilme,tabClienti,tabInchirieri);
//
        //Button btn = new Button();
        //btn.setText("Which tab?");
        //Label label = new Label();
        //btn.setOnAction((evt)-> {
        //label.setText(tabPane.getSelectionModel().getSelectedItem().getText());
        //});
        VBox vBox = new VBox(10);
        vBox.getChildren().add(tabPane);
        Scene scene = new Scene(vBox);
        primaryStage.setScene(scene);
        primaryStage.show();

        //you can also watch the selectedItemProperty
        tabPane.getSelectionModel().selectedItemProperty().addListener((obs,oldValue,newValue)->{
            primaryStage.setTitle(newValue.getText());
            //tabFilme.setContent(view.getView());
            //tabClienti.setContent(paneClienti);
            //tabInchirieri.setContent(paneInchirieri);
        });
    }
    private static MVCClientService getClientsService(){
        ClientRepoSerialized repo = new ClientRepoSerialized(".\\src\\fisierSerializat.txt", new ValidatorClient());
        MVCClientService service = new MVCClientService(repo);
        return service;
    }
    private static MVCInchirieriService getServiceInchirieri(){
        FileRepoInchirieriFilme repo = new FileRepoInchirieriFilme(".\\src\\inchirieri.txt", new ValidatorInchiriere());
        MVCInchirieriService service = new MVCInchirieriService(repo);
        return service;
    }

    private static MVCMovieService getMoviesService() {
        FileMovieRepo repo = new FileMovieRepo(".\\src\\filme.txt", new ValidatorMovie());
        MVCMovieService service = new MVCMovieService(repo);
        return service;
    }
}

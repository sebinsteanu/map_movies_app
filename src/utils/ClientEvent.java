package utils;

import Model.Client;

/**
 * Created by sebi on 12/5/2016.
 */
public class ClientEvent implements Event {
        private METype type;
        private Client data, oldData;

        public ClientEvent(METype type, Client data) {
            this.type = type;
            this.data = data;
        }
        public ClientEvent(METype type, Client data, Client oldData) {
            this.type = type;
            this.data = data;
            this.oldData=oldData;
        }

        public METype getType() {
            return type;
        }

        public Client getData() {
            return data;
        }

        public Client getOldData() {return oldData;}

}

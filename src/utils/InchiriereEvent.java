package utils;

import Model.InchiriereFilm;

/**
 * Created by sebi on 12/7/2016.
 */
public class InchiriereEvent implements Event {

    private METype type;
    private InchiriereFilm data, oldData;

    public InchiriereEvent(METype type, InchiriereFilm data) {
        this.type = type;
        this.data = data;
    }
    public InchiriereEvent(METype type, InchiriereFilm data, InchiriereFilm oldData) {
        this.type = type;
        this.data = data;
        this.oldData=oldData;
    }

    public METype getType() {
        return type;
    }

    public InchiriereFilm getData() {
        return data;
    }

    public InchiriereFilm getOldData() {return oldData;}
}

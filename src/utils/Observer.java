package utils;

/**
 * Created by sebi on 12/3/2016.
 */

public interface Observer<E extends Event> {
        void update(E e);
}

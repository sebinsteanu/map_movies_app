package utils;

/**
 * Created by sebi on 12/3/2016.
 */
public interface Observable<E extends Event> {
    void addObserver(Observer<E> observer);
    void removeObserver(Observer<E> observer);
    void notifyObservers( E t);
}
package utils;

import Model.Movie;

/**
 * Created by sebi on 12/3/2016.
 */
public class MovieEvent implements Event{
    private METype type;
    private Movie data, oldData;

    public MovieEvent(METype type, Movie data) {
        this.type = type;
        this.data = data;
    }
    public MovieEvent(METype type, Movie data, Movie oldData) {
        this.type = type;
        this.data = data;
        this.oldData=oldData;
    }

    public METype getType() {
        return type;
    }

    public Movie getData() {
        return data;
    }

    public Movie getOldData() {return oldData;}
}

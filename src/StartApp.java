import Controller.MovieController;
import Model.Movie;
import Repository.*;
import Repository.xml.SAXMoviesRepo;
import View.MoviesUI;

import java.util.ArrayList;

/**
 * Created by sebi on 10/13/2016.
 */
public class StartApp {
    public static void main(String[] argv){
        String simple_file = "D:\\map\\Movies_app\\src\\filme.txt";
        String serialized_file = "D:\\map\\Movies_app\\src\\fisierSerializat.txt";


        //AppTest.ClientTest();
        //AppTest.MovieTest();
        //AppTest.testMovieRepository();

        try {
            MoviesUI UI = new MoviesUI();
            MovieController controller = new MovieController();
            ValidatorMovie val = new ValidatorMovie();
            ValidatorClient valC = new ValidatorClient();

            UI.setInchirieriFilmeManager(controller);
            controller.setFilmRepository(new FileMovieRepo(simple_file,val));
            controller.setClientRepository(new ClientRepoSerialized(serialized_file,valC));


            //UI.showUI();

            //SectionData sectionData= new SectionData("src\\movies.xml");
            ArrayList<Movie> list = new ArrayList<>();
            for (Movie m : controller.GetAllFilms())
                list.add(m);
            //sectionData.setList(list);

            SAXMoviesRepo repo = new SAXMoviesRepo(val,"src\\movies.xml");

            for (Movie m : controller.GetAllFilms())
                repo.save(m);
            //repo.save(new Movie(32,"hor","re","last"));
            for (Movie m : repo.findAll())
                System.out.println(m);



        } catch (Exception e) {
            System.out.println("The App has encountered an error: "+e.getMessage());
        }
    }
}

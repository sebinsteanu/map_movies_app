import Controller.MVCClientController;
import Repository.ClientRepoSerialized;
import Repository.ValidatorClient;
import View.ClientiViewFXML;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import service.MVCClientService;

/**
 * Created by sebi on 12/3/2016.
 */
public class MainFXML_Clients extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("FXML View");
        FXMLLoader loader = new FXMLLoader(getClass().getResource("View/clienti.fxml"));
        Pane pane = loader.load();

        ClientiViewFXML ctrl = loader.getController();
        ctrl.setService(getService());

        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args){
        launch(args);
    }

    static MVCClientService getService(){
        ClientRepoSerialized repo = new ClientRepoSerialized("D:\\map\\Movies_app\\src\\fisierSerializat.txt", new ValidatorClient());
        MVCClientService service = new MVCClientService(repo);
        return service;
    }
}

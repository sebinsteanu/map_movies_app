package View;

import Controller.MVCMovieController;
import Model.Movie;
import Repository.RepositoryException;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import utils.Event;
import utils.Observer;

/**
 * Created by sebi on 12/2/2016.
 */

public class MoviesView implements Observer<Event>{
    BorderPane pane;
    TextField txtMovieId,txtMovieGen,txtMovieRegizor,txtMovieTitlu, txtFilter;
    ToggleGroup group;
    RadioButton genBtn, regizorBtn;
    MVCMovieController controller;

    public MoviesView(MVCMovieController controller) {
        this.controller = controller;
        initView();
    }
    private void initView(){
        pane = new BorderPane();
        pane.setRight(manageMovie());
        pane.setLeft(infoMovie());
    }

    protected GridPane manageMovie(){
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(15);
        gridPane.setPadding(new Insets(10,20,20,20));

        //id
        Label labelId = new Label("ID: ");
        txtMovieId = new TextField();
        gridPane.add(labelId,0,0);
        gridPane.add(txtMovieId,1,0);

        //titlu
        Label labelTitle = new Label("Titlu: ");
        txtMovieTitlu = new TextField();
        gridPane.add(labelTitle,0,1);
        gridPane.add(txtMovieTitlu,1,1);

        //gen
        Label labelGen = new Label("Gen: ");
        txtMovieGen = new TextField();
        gridPane.add(labelGen,0,2);
        gridPane.add(txtMovieGen,1,2);

        //regizor
        Label labelRegizor = new Label("Regizor: ");
        txtMovieRegizor = new TextField();
        gridPane.add(labelRegizor,0,3);
        gridPane.add(txtMovieRegizor,1,3);

        Button addMovie=new Button("Add");
        Button deleteMovie=new Button("Delete");
        Button updateMovie=new Button("Update");
        HBox hbBtns = new HBox(10);
        hbBtns.setAlignment(Pos.BOTTOM_RIGHT);


        hbBtns.getChildren().addAll(addMovie, deleteMovie,updateMovie);
        gridPane.add(hbBtns,0,4,2,1);

        VBox lyFiltrare;
        group = new ToggleGroup();
        genBtn = new RadioButton("gen");
        regizorBtn = new RadioButton("regizor");
        genBtn.setToggleGroup(group);
        regizorBtn.setToggleGroup(group);
        txtFilter = new TextField();
        txtFilter.setMaxWidth(txtMovieTitlu.getMaxWidth());
        HBox groupFilter = new HBox(genBtn,regizorBtn);
        groupFilter.setSpacing(20);
        Button btnFilter = new Button("filtreaza");

        lyFiltrare = new VBox(txtFilter,groupFilter,btnFilter);
        lyFiltrare.setSpacing(20);
        gridPane.add(lyFiltrare,0,6,20,20);

        addMovie.setOnAction(x->addButton());
        deleteMovie.setOnAction(x->handleDelete());
        updateMovie.setOnAction(x->handleUpdateMovie());
        btnFilter.setOnAction(x->filter());
        return gridPane;
    }

    private TableView<Movie> table = new TableView<>();
    protected StackPane infoMovie(){
        StackPane pane = new StackPane();
        initMoviesTable();
        pane.getChildren().add(table);

        return pane;
    }
    private void initMoviesTable(){
        TableColumn<Movie,Integer> colId = new TableColumn<>("ID");
        TableColumn<Movie, String> colTitle = new TableColumn<>("Titlu");
        TableColumn<Movie, String> colGen = new TableColumn<>("Gen");
        TableColumn<Movie, String> colRegizor = new TableColumn<>("Regizor");

        table.getColumns().addAll(colId, colTitle, colGen, colRegizor);

        colId.setCellValueFactory(new PropertyValueFactory<Movie,Integer>("ID"));
        colTitle.setCellValueFactory(new PropertyValueFactory<Movie,String>("Titlu"));
        colGen.setCellValueFactory(new PropertyValueFactory<Movie, String>("gen"));
        colRegizor.setCellValueFactory(new PropertyValueFactory<Movie, String>("regizor"));

        table.setItems(controller.getmoviesModel());

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        // Listen for selection changes and show the SortingTask details when changed.
        table.getSelectionModel().selectedItemProperty().addListener(
                (observable,oldvalue,newValue)->showMovieDetails(newValue) );
    }
    private void showMovieDetails(Movie movie){
        if(movie == null){
            clearFields();
        }
        else {
            txtMovieId.setText(""+movie.getID());
            txtMovieTitlu.setText(movie.getTitlu());
            txtMovieGen.setText(movie.getGen());
            txtMovieRegizor.setText(movie.getRegizor());
        }
    }
    private void clearFields(){
        txtMovieId.setText("");
        txtMovieRegizor.setText("");
        txtMovieGen.setText("");
        txtMovieTitlu.setText("");
    }

    public BorderPane getView(){
        return pane;
    }

    private void handleDelete() {
        int index = table.getSelectionModel().getSelectedIndex();
        if(index<0){
            showErrorMessage("Trebuie sa selectazi un film din tabel!");
        }
        Movie m =table.getSelectionModel().getSelectedItem();
        controller.deletemovie(m);
    }
    private void filter(){
        Stage filterStage;
        TableView<Movie> filterTable = new TableView<>();
        TableColumn<Movie,Integer> colId = new TableColumn<>("ID");
        TableColumn<Movie, String> colTitle = new TableColumn<>("Titlu");
        TableColumn<Movie, String> colGen = new TableColumn<>("Gen");
        TableColumn<Movie, String> colRegizor = new TableColumn<>("Regizor");

        filterTable.getColumns().addAll(colId, colTitle, colGen, colRegizor);

        colId.setCellValueFactory(new PropertyValueFactory<Movie,Integer>("ID"));
        colTitle.setCellValueFactory(new PropertyValueFactory<Movie,String>("Titlu"));
        colGen.setCellValueFactory(new PropertyValueFactory<Movie, String>("gen"));
        colRegizor.setCellValueFactory(new PropertyValueFactory<Movie, String>("regizor"));

        if (regizorBtn.isSelected()) {
            filterTable.setItems(controller.getmoviesByRegizor(txtFilter.getText()));
        }
        else if(genBtn.isSelected()){
            filterTable.setItems(controller.getmoviesByGen(txtFilter.getText()));
        }
        Scene scene = new Scene(filterTable);
        filterStage = new Stage();
        filterStage.setScene(scene);
        filterStage.show();
    }
    private void addButton(){
        String id =txtMovieId.getText();
        String title = txtMovieTitlu.getText();
        String gen = txtMovieGen.getText();
        String regizor = txtMovieRegizor.getText();
        try{
            controller.addMovie(Integer.parseInt(id),title,gen,regizor);
        }
        catch (NumberFormatException e){
            showErrorMessage(e.getMessage());
        }
        catch (RepositoryException e){
            showErrorMessage(e.getMessage());
        }
    }
    private void handleUpdateMovie(){
        int index = table.getSelectionModel().getSelectedIndex();
        if(index<0){
            showErrorMessage("Trebuie sa selectazi un film din tabel!");
        }
        Movie oldMovie = table.getSelectionModel().getSelectedItem();
        String id = txtMovieId.getText();
        String title = txtMovieTitlu.getText();
        String gen = txtMovieGen.getText();
        String regizor = txtMovieRegizor.getText();
        try{
            controller.updateMovie(oldMovie,Integer.parseInt(id),title,gen,regizor);
        }
        catch (NumberFormatException e){
            showErrorMessage("Numarul de la id trebuie sa fie intreg: "+e.getMessage());
        }
        catch (RepositoryException rEx){
            showErrorMessage(rEx.getMessage());
        }
    }

    static void showErrorMessage(String text){
        Alert alert=new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Mesaj eroare");
        alert.setContentText(text);
        alert.showAndWait();
    }

    @Override
    public void update(Event event) {
        table.getItems().clear();
        table.setItems(controller.getmoviesModel());
    }
}

package View;

import Controller.MovieController;
import Model.Client;
import Model.Movie;
import Repository.ValidationException;
import Repository.RepositoryException;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;


/**
 * Created by sebi on 10/13/2016.
 */
public class MoviesUI {

    private MovieController manager;

    public void setInchirieriFilmeManager(MovieController manager){
        this.manager = manager;
    }

    public void printCommands() {
        System.out.println("1. Add film");
        System.out.println("2. Remove film");
        System.out.println("3. Modify film");
        System.out.println("4. List films");
        System.out.println("5. Add Client");
        System.out.println("6. Remove Client");
        System.out.println("7. Modify Client");
        System.out.println("8. List Clients");
        System.out.println("9. Filter movies by gen, sorted by autor");
        System.out.println("10. Filter movies by regizor, sorted by title");
        System.out.println("11. Filter clients by nume, sorted by address");
        System.out.println("12. Filter clients by adresa, sorted by name");
        System.out.println("0. Exit");
    }

    private String input(String mesaj) {
        Scanner input = new Scanner(System.in);
        System.out.print(mesaj);
        return input.nextLine();
    }

    public void showUI() {
        String cmd = "";
        boolean isRunning = true;

        while (isRunning) {
            printCommands();

            cmd = input("Enter command: ");
            System.out.println();

            switch (cmd) {
                case "1":
                    createFilm();
                    break;
                case "2":
                    removeFilm();
                    break;
                case "3":
                    modifyFilm();
                    break;
                case "4":
                    listFilms();
                    break;
                case "5":
                    createClient();
                    break;
                case "6":
                    removeClient();
                    break;
                case "7":
                    modifyClient();
                    break;
                case "8":
                    listClients();
                    break;
                case "9":
                    filtrare_filme_gen();
                    break;
                case "10":
                    filtrare_filme_regizor();
                    break;
                case "11":
                    filtrare_clienti_nume();
                    break;
                case "12":
                    filtrare_clienti_adr();
                    break;
                case "0":
                    System.out.println("Bye");
                    isRunning = false;
                    break;

                default:
                    System.out.println("Invalid command!");
                    break;
            }
            System.out.println("\n--------------");

            if (cmd == "0" || isRunning == false)
                break;
        }
    }

    private void createFilm() {
        try {
            int id = Integer.parseInt(input("Film ID = "));
            String titlu = input("Film title = ");
            String gen = input("Film gen = ");
            String autor = input("Film autor = ");

            Movie film = manager.addFilm(id, titlu, gen, autor);

            System.out.println("Film created: " + film.getID());
        }
        catch (ValidationException ex) {
            System.out.println("Invalid film info: " + ex.getMessage());
        }
        catch (RepositoryException ex) {
            System.out.println("Repo exception: " + ex.getMessage());
        }
    }

    private void removeFilm(){
        try {
            int id = Integer.parseInt(input("Film ID = "));
            Movie film = manager.removeFilm(id);
            System.out.println("Film deleted: " + film.getID());
        }
        catch (ValidationException ex) {
            System.out.println("Invalid film info: " + ex.getMessage());
        }
        catch (RepositoryException ex) {
            System.out.println("Repo exception: " + ex.getMessage());
        }
    }

    private void modifyFilm(){
        try {
            int id = Integer.parseInt(input("Film ID = "));
            String titlu = input("Film title = ");
            String gen = input("Film gen = ");
            String autor = input("Film autor = ");

            Movie film = manager.modifyFilm(id, titlu, gen, autor);


            System.out.println("Film modified: " + film.getID());
        }
        catch (ValidationException ex) {
            System.out.println("Invalid film info: " + ex.getMessage());
        }
        catch (RepositoryException ex) {
            System.out.println("Repo exception: " + ex.getMessage());
        }
    }

    private void listFilms() {
        Iterable<Movie> films = manager.GetAllFilms();

        for(Movie film: films)
        {
            System.out.println(film.getID()+", "+film.getTitlu()+", "+film.getGen()+", "+film.getRegizor());
        }
    }

    private<E> void afisare_lista(List<E> lista){
        for (E el:lista)
            System.out.println(el);
    }

    private void createClient() {
        try {
            int id = Integer.parseInt(input("Client ID = "));
            String nume = input("Client name = ");
            String adresa = input("Client address = ");

            Client client = manager.addClient(id, nume, adresa);

            System.out.println("Client created: " + client.getID());
        }
        catch (ValidationException ex) {
            System.out.println("Invalid Client info: " + ex.getMessage());
        }
        catch (RepositoryException ex) {
            System.out.println("Repo exception: " + ex.getMessage());
        }
    }

    private void removeClient(){
        try {
            int id = Integer.parseInt(input("Client ID = "));
            Client c = manager.removeClient(id);
            System.out.println("Client deleted: " + c.getID());
        }
        catch (ValidationException ex) {
            System.out.println("Invalid Client info: " + ex.getMessage());
        }
        catch (RepositoryException ex) {
            System.out.println("Repo exception: " + ex.getMessage());
        }
    }

    private void modifyClient(){
        try {
            int id = Integer.parseInt(input("Client ID = "));
            String name = input("Client name = ");
            String address = input("Client address = ");
            Client client = manager.modifyClient(id, name, address);

            System.out.println("Client modified: " + client.getID());
        }
        catch (ValidationException ex) {
            System.out.println("Invalid client info: " + ex.getMessage());
        }
        catch (RepositoryException ex) {
            System.out.println("Repo exception: " + ex.getMessage());
        }
    }

    private void filtrare_filme_gen(){
        String gen = input("Genul dorit : ");
        Iterable<Movie> itList= manager.GetAllFilms();
        List<Movie> finalList = new ArrayList<>();
        for(Movie m: itList)
            finalList.add(m);
        List<Movie> lista = manager.filterByGen(finalList,gen);
        Comparator<Movie> comp = ((a,b)->{return a.getRegizor().compareTo(b.getRegizor());});
        List<Movie> lista_sortata = lista
                .stream()
                .sorted(comp)
                .collect(Collectors.toList());

        afisare_lista(lista_sortata);
    }
    private void filtrare_filme_regizor(){
        String reg = input("Regizorul dorit : ");
        Iterable<Movie> itList= manager.GetAllFilms();
        List<Movie> finalList = new ArrayList<>();
        for(Movie m: itList)
            finalList.add(m);
        List<Movie> lista = manager.filterByRegizor(finalList,reg);
        lista.sort((f1,f2)->{return f1.getTitlu().compareTo(f2.getTitlu());});
        afisare_lista(lista);
    }
    private void filtrare_clienti_nume(){
        String nume = input("Numele dorit : ");
        Iterable<Client> itList= manager.GetAllClients();
        List<Client> finalList = new ArrayList<>();
        for(Client c: itList)
            finalList.add(c);
        List<Client> lista = manager.filterByName(finalList,nume);
        Comparator<Client> comparator = ((c1,c2)->{return c1.getAdresa().compareTo(c2.getAdresa());});
        List<Client> lista2= lista
                .stream()
                .sorted(comparator).collect(Collectors.toList());

        afisare_lista(lista2);
    }
    private void filtrare_clienti_adr(){
        String adr = input("Numele dorit : ");
        Iterable<Client> itList= manager.GetAllClients();
        List<Client> finalList = new ArrayList<>();
        for(Client c: itList)
            finalList.add(c);
        List<Client> lista = manager.filterByAddress(finalList,adr);
        lista.sort((c1,c2)->{return c1.getAdresa().compareTo(c2.getAdresa());});
        afisare_lista(lista);
    }

    private void listClients() {
        Iterable<Client> clients = manager.GetAllClients();
        for(Client c: clients)
        {
            System.out.println(c);
        }
    }
}

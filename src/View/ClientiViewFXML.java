package View;

import Repository.RepositoryException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import Model.Client;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import service.MVCClientService;
import utils.ClientEvent;
import utils.Event;
import utils.Observer;

        import java.net.URL;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
  * Created by sebi on 12/5/2016.
 */
public class ClientiViewFXML implements Initializable, Observer<Event> {
    @FXML private TableView<Client> table;
    @FXML private TextField txtId,txtName, txtAdresa, txtFilter;
    @FXML private TextArea execMessages;
    @FXML private RadioButton numeBtn, adresaBtn;
    @FXML private ToggleGroup groupFilter ;

    private MVCClientService service;
    @Override
    public void update(Event event) {
        ClientEvent clientEvent = (ClientEvent) event;
        switch (clientEvent.getType()) {
            case ADD:
                appendMessage("S-a adaugat un nou client");
                //table.getItems().add(clientEvent.getData());
                break;
            case UPDATE:
                appendMessage("Datele clientului au fost actualizate");
                //table.getItems().remove(clientEvent.getOldData());
                //table.getItems().add(clientEvent.getData());
                break;
            case DELETE:
                appendMessage("Clientul "+txtId.getText()+" a fost sters");
                //table.getItems().remove(clientEvent.getData());
                break;
        }
        initData();
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        table.getSelectionModel().selectedItemProperty().addListener((observable,oldvalue,newValue)->showClientDetails(newValue) );
        execMessages.setEditable(false);
    }
    public ClientiViewFXML( ) {
    }

    private void showClientDetails(Client value){
        if (value==null)
            clearFields();
        else{
            txtId.setText(""+value.getID());
            txtName.setText(value.getNume());
            txtAdresa.setText(value.getAdresa());
        }
    }
    private void initData() {
        table.getItems().clear();
        //table.setItems(service.getClientsModel());
        for (Client c : service.getAll()) {
            table.getItems().add(c);
        }
    }
    @FXML private void clearFields(){
        txtName.setText("");
        txtId.setText("");
        txtAdresa.setText("");
    }

    public void setService(MVCClientService service){
        this.service = service;
        service.addObserver(this);
        initData();
    }

    private void appendMessage(String text){
        execMessages.appendText(text+"\n");
    }
    static void showErrorMessage(String text){
        Alert alert=new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Putina atentie va rog!");
        alert.setContentText(text);
        alert.showAndWait();
    }

    @FXML private void handleFilterBtn(ActionEvent ev){
        Stage filterStage;

        TableView<Client> filterTable = new TableView<>();
        TableColumn<Client,Integer> colId = new TableColumn<>("ID");
        TableColumn<Client, String> colName = new TableColumn<>("Nume");
        TableColumn<Client, String> colAdr = new TableColumn<>("Adresa");

        filterTable.getColumns().addAll(colId, colName, colAdr);
        colId.setCellValueFactory(new PropertyValueFactory<Client,Integer>("ID"));
        colName.setCellValueFactory(new PropertyValueFactory<Client,String>("nume"));
        colAdr.setCellValueFactory(new PropertyValueFactory<Client, String>("adresa"));

        Scene scene = new Scene(filterTable);
        if (numeBtn.isSelected()){
            ObservableList<Client> filterList;
            filterList = makeCollection(service.getAll()).stream().filter(x->x.getNume().equals(txtFilter.getText())).collect(Collectors.toCollection(FXCollections::observableArrayList));
            filterTable.setItems(filterList);
        }
        else if(adresaBtn.isSelected()){
            ObservableList<Client> filterList;
            filterList = makeCollection(service.getAll()).stream().filter(x->x.getAdresa().equals(txtFilter.getText())).collect(Collectors.toCollection(FXCollections::observableArrayList));
            filterTable.setItems(filterList);
        }
        filterStage = new Stage();
        filterStage.setScene(scene);
        filterStage.show();
    }
    public static <E> Collection<E> makeCollection(Iterable<E> iter) {
        Collection<E> list = new ArrayList<E>();
        for (E item : iter) {
            list.add(item);
        }
        return list;
    }
    @FXML
    public void handleAddBtn(ActionEvent ae) {
        String id = txtId.getText();
        String name = txtName.getText();
        String adresa = txtAdresa.getText();
        try{
            service.addClient(new Client(Integer.parseInt(id),name,adresa));
        }
        catch (NumberFormatException e){
            showErrorMessage(e.getMessage());
        }
        catch (RepositoryException e){
            showErrorMessage(e.getMessage());
        }
    }
    @FXML
    private void handleDeleteBtn(ActionEvent ae){
        int index = table.getSelectionModel().getSelectedIndex();
        if(index<0){
            showErrorMessage("Trebuie sa selectazi un client din tabel!");
        }
        Client c = table.getSelectionModel().getSelectedItem();
        try {
            service.deleteClient(c);
            clearFields();
        }
        catch (RepositoryException ex){
            showErrorMessage(ex.getMessage());
        }
    }
    @FXML
    private void handleUpdateBtn(ActionEvent ae){
        int index = table.getSelectionModel().getSelectedIndex();
        if(index<0){
            showErrorMessage("Trebuie sa selectazi un client din tabel!");
        }
        try {
            Client c = table.getSelectionModel().getSelectedItem();
            try {
                service.updateClient(c, new Client(Integer.parseInt(txtId.getText()), txtName.getText(), txtAdresa.getText()));
                clearFields();
            }
            catch (RepositoryException ex){
                showErrorMessage(ex.getMessage());
            }
        }
        catch (NumberFormatException ex){
            showErrorMessage(ex.getMessage());
        }
        catch (Exception e){
            showErrorMessage(e.getMessage());
        }
    }
}

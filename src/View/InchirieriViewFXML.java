package View;

import Model.Client;
import Model.InchiriereFilm;
import Model.Movie;
import Repository.RepositoryException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import service.MVCClientService;
import service.MVCInchirieriService;
import service.MVCMovieService;
import utils.*;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by sebi on 12/7/2016.
 */
public class InchirieriViewFXML implements Initializable, Observer<Event>{

    @FXML private TableView<InchiriereFilm> tableInchirieri;
    @FXML private TableView<Client> tableClients;
    @FXML private TableView<Movie> tableMovies;

    @FXML private TextField txtIdClient,txtIdMovie, txtData;

    private MVCInchirieriService service;
    private MVCMovieService movieService;
    private MVCClientService clientService;

    public InchirieriViewFXML(){

    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tableInchirieri.getSelectionModel().selectedItemProperty().addListener((observable,oldVal,newVal)-> showInchiriere(newVal));
    }

    private void showInchiriere(InchiriereFilm inchiriereFilm){
        if (inchiriereFilm == null){
            clerFields();
        }
        else {
            txtIdClient.setText(""+inchiriereFilm.getIdClient());
            txtIdMovie.setText(""+inchiriereFilm.getIdMovie());
            txtData.setText(inchiriereFilm.getDate());
            Client c =clientService.getById(inchiriereFilm.getIdClient());
            Movie m = movieService.getById(inchiriereFilm.getIdMovie());
            tableMovies.getSelectionModel().select(m);
            tableClients.getSelectionModel().select(c);
        }
    }
    private void initDataTable(){
        tableInchirieri.getItems().clear();
        tableClients.getItems().clear();
        tableMovies.getItems().clear();
        service.getAll().forEach(I->tableInchirieri.getItems().add(I));
        clientService.getAll().forEach(C->tableClients.getItems().add(C));
        movieService.getAll().forEach(M->tableMovies.getItems().add(M));
    }

    public void setService(MVCInchirieriService inchirieriService, MVCClientService clientService, MVCMovieService movieService){
        this.service = inchirieriService;
        this.clientService = clientService;
        this.movieService = movieService;
        service.addObserver(this);
        clientService.addObserver(this);
        movieService.addObserver(this);

        initDataTable();
    }

    @FXML void handleInchiriazaBtn(){
        try{
            int idMovie = Integer.parseInt(txtIdMovie.getText());
            int idClient = Integer.parseInt(txtIdClient.getText());
            if(clientService.getById(idClient)!= null && movieService.getById(idMovie)!=null)
                service.addInchiriere(new InchiriereFilm(service.getFreeId(),idClient,idMovie,txtData.getText()));
            else
                throw new RepositoryException("Id-urile nu fac parte din listele disponibile!");
        }
        catch (NumberFormatException ex){
            showErrorMessage(ex.getMessage());
        }
        catch (Exception e){
            showErrorMessage(e.getMessage());
        }
    }
    @FXML void handleReturneaza(){
        try{
            int idMovie = Integer.parseInt(txtIdMovie.getText());
            int idClient = Integer.parseInt(txtIdClient.getText());
            service.deleteInchiriere(new InchiriereFilm(service.getFreeId(),idClient,idMovie,txtData.getText()));
        }
        catch (NumberFormatException ex){
            showErrorMessage(ex.getMessage());
        }
        catch (Exception e){
            showErrorMessage(e.getMessage());
        }
    }

    @FXML private void clerFields(){
        txtData.setText("");
        txtIdMovie.setText("");
        txtIdClient.setText("");
    }
    private static void showErrorMessage(String msg){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setContentText(msg);
        alert.setTitle("Socoteala din targ..");
        alert.showAndWait();
    }

    @Override
    public void update(Event inchiriereEvent) {
        initDataTable();
    }
}
